﻿using DAL.Context;
using Microsoft.EntityFrameworkCore;
using System;
using Bogus;

namespace BLL.Tests
{
    public class SeedDataFixture : IDisposable
    {
        public ProjectContext Context { get; private set; }

        public int TEAM_COUNT { get; set; } = 1;
        public int USER_COUNT { get; set; } = 1;
        public int PROJECT_COUNT { get; set; } = 1;
        public int TASK_COUNT { get; set; } = 1;

        public SeedDataFixture()
        {
            var options = new DbContextOptionsBuilder<ProjectContext>()
                            .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                            .Options;
            Context = new ProjectContext(options);
            
        }

        public void Seed()
        {
            Randomizer.Seed = new Random(13);

            var teams = ModelBuilderExtensions.GenerateRandomTeams(TEAM_COUNT);
            var users = ModelBuilderExtensions.GenerateRandomUsers(teams, USER_COUNT);
            var projects = ModelBuilderExtensions.GenerateRandomProjects(users, teams, PROJECT_COUNT);
            var tasks = ModelBuilderExtensions.GenerateRandomTasks(users, projects, TASK_COUNT);

            Context.Teams.AddRange(teams);
            Context.Users.AddRange(users);
            Context.Projects.AddRange(projects);
            Context.Tasks.AddRange(tasks);

            Context.SaveChanges();
        }

        public void Dispose()
        {
            Context.Dispose();
        }
    }
}
