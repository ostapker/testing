﻿using Common.DTOs.Project;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Client.Interfaces
{
    public interface IProjectService
    {
        Task<IEnumerable<ProjectDTO>> GetProjectsAsync();

        Task<ProjectDTO> GetProjectByIdAsync(int id);

        Task<ProjectDTO> CreateProjectAsync(ProjectCreateDTO projectCreateModel);

        Task<ProjectDTO> UpdateProjectAsync(ProjectUpdateDTO projectUpdateModel);

        Task DeleteProjectByIdAsync(int id);

        Task<Dictionary<ProjectDTO, int>> GetProjects_TaskCountByUser(int id);

        Task<IEnumerable<ProjectDetailedInfoDTO>> GetProjectsInfo();
    }
}
