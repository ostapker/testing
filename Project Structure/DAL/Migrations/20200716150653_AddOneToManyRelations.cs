﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DAL.Migrations
{
    public partial class AddOneToManyRelations : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ProjectEntityId",
                table: "Tasks",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Tasks_ProjectEntityId",
                table: "Tasks",
                column: "ProjectEntityId");

            migrationBuilder.AddForeignKey(
                name: "FK_Tasks_Projects_ProjectEntityId",
                table: "Tasks",
                column: "ProjectEntityId",
                principalTable: "Projects",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Tasks_Projects_ProjectEntityId",
                table: "Tasks");

            migrationBuilder.DropIndex(
                name: "IX_Tasks_ProjectEntityId",
                table: "Tasks");

            migrationBuilder.DropColumn(
                name: "ProjectEntityId",
                table: "Tasks");
        }
    }
}
