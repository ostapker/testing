﻿using System;

namespace Common.DTOs.User
{
    public class UserCreateDTO
    {
        public int? TeamId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public DateTime Birthday { get; set; }
    }
}
