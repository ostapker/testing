﻿using DAL.Entities.Abstract;
using System;
using System.Collections.Generic;

namespace DAL.Entities
{
    public class ProjectEntity : BaseEntity
    {
        public ProjectEntity()
        {
            Tasks = new HashSet<TaskEntity>();
        }

        public int AuthorId { get; set; }
        public virtual UserEntity Author { get; set; }

        public int TeamId { get; set; }
        public virtual TeamEntity Team { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public DateTime CreatedAt { get; set; }

        public DateTime Deadline { get; set; }

        public virtual ICollection<TaskEntity> Tasks { get; private set; }

    }
}
