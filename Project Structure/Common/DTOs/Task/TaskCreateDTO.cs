﻿using Common.Enums;
using System;

namespace Common.DTOs.Task
{
    public class TaskCreateDTO
    {
        public int ProjectId { get; set; }

        public int PerformerId { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public DateTime FinishedAt { get; set; }
    }
}
