﻿using DAL.Entities.Abstract;
using System;
using System.Threading.Tasks;

namespace DAL.UnitOfWork.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        IRepository<TEntity> Set<TEntity>()  where TEntity : BaseEntity;

        void SaveChanges();

        Task SaveChangesAsync();
    }
}
