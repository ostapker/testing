﻿using DAL.Entities.Abstract;
using System;
using System.Collections.Generic;

namespace DAL.Entities
{
    public class TeamEntity : BaseEntity
    {
        public TeamEntity()
        {
            Users = new HashSet<UserEntity>();
            Projects = new HashSet<ProjectEntity>();
        }

        public string Name { get; set; }

        public DateTime CreatedAt { get; set; }

        public virtual ICollection<UserEntity> Users { get; private set; }
        public virtual ICollection<ProjectEntity> Projects { get; private set; }
    }
}
