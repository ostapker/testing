﻿using DAL.Entities;
using Microsoft.EntityFrameworkCore;

namespace DAL.Context
{
    public class ProjectContext : DbContext
    {  

        public ProjectContext(DbContextOptions<ProjectContext> options)
            : base(options) 
        { }

        public DbSet<TaskEntity> Tasks { get; private set; }
        public DbSet<TeamEntity> Teams { get; private set; }
        public DbSet<UserEntity> Users { get; private set; }
        public DbSet<ProjectEntity> Projects { get; private set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Configure();

            modelBuilder.Seed();
        }
    }
}
